import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { PROFILE } from "./shared/entity-url-mapping";
import { UserInfoLayoutComponent } from "./shared/layouts/user-info-layout.component";

const routes: Routes = [
  {
    path: "",
    component: UserInfoLayoutComponent,
    loadChildren: () => import('./modules/user-info/user-info.module').then(mod => mod.UserInfoModule)
  },
  {
    path: PROFILE,
    loadChildren: () =>
      import("./modules/profile/profile.module").then((m) => m.ProfileModule),
  },
  {
    path: "**",
    redirectTo: "/",
    pathMatch: "full",
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
