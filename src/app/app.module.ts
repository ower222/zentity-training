import { APP_INITIALIZER, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { UserInfoLayoutComponent } from './shared/layouts/user-info-layout.component';
import { HttpClientModule } from '@angular/common/http';
import { StoreModule } from '@ngrx/store';
import { dataReducer } from './store/data.reducer';
import { DataStoreFacade } from './store/data.facade';
import { EffectsModule } from '@ngrx/effects';
import { DataEffects } from './store/data.effects';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';

export function initApplication(dataStoreFacade: DataStoreFacade) {
  return ()  => { 
  return new Promise((resolve) => {
    dataStoreFacade.retrieveUserData();
    resolve(true);
  })
};
}

@NgModule({
  declarations: [
    AppComponent,
    UserInfoLayoutComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    StoreModule.forRoot({ data: dataReducer}),
    EffectsModule.forRoot([DataEffects]),
    StoreDevtoolsModule.instrument({
      maxAge: 25,
      autoPause: true
    })
  ],
  providers: [
    DataEffects,
    DataStoreFacade,
    {
      provide: APP_INITIALIZER,
      useFactory: initApplication,
      deps: [DataStoreFacade],
      multi: true
   }],
  bootstrap: [AppComponent]
})
export class AppModule { }
