import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { ProfileGuardService } from "./guards/profile-guard.service";
import { ProfileComponent } from "./profile/profile.component";

const routes: Routes = [
  { path: "", component: ProfileComponent, canActivate: [ProfileGuardService] },
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ProfileRoutingModule {}
