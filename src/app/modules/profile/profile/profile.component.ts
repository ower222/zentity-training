import { Component, OnInit } from "@angular/core";
import { FormBuilder, Validators } from "@angular/forms";
import { takeUntil, tap } from "rxjs";
import { BaseForm } from "src/app/shared/components/base-form.component";
import { DataStoreFacade } from "src/app/store/data.facade";
import { Location } from "@angular/common";
import { SocialNetworks } from "src/app/store/data.model";
import { Router } from "@angular/router";

@Component({
  selector: "app-profile",
  templateUrl: "./profile.component.html",
  styleUrls: ["./profile.component.scss"],
  host: { class: "form-component" },
})
export class ProfileComponent extends BaseForm implements OnInit {
  public socialMedia: SocialNetworks[] = [];

  constructor(
    private readonly _fb: FormBuilder,
    private readonly _dataStoreFacade: DataStoreFacade,
    private readonly _location: Location,
    private readonly _router: Router
  ) {
    super();
    this.form = this._fb.group({
      name: ["", Validators.required],
      userName: ["", Validators.required],
      address: this._fb.group({
        address: [""],
        city: [""],
        postalCode: [""],
      }),
      contact: this._fb.group({
        email: [""],
        phone: [""],
        socialNetworksString: [""],
      }),
    });
  }

  ngOnInit(): void {
    this._dataStoreFacade.userDetail$
      .pipe(
        tap((userData) => {
          this.form?.patchValue(userData);
          this.socialMedia = userData.contact.socialNetworks;
        }),
        takeUntil(this.ngUnsubscribe)
      )
      .subscribe();
  }

  public updateProfile(): void {
    this._dataStoreFacade.updateProfileData(this.form.getRawValue());
  }

  public back(): void {
    this._location.back();
  }

  public logout(): void {
    this._dataStoreFacade.logout();
    this._router.navigate(["/"]);
  }
}
