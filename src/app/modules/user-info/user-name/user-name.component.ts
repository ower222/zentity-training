import { Component } from "@angular/core";
import { FormBuilder, Validators } from "@angular/forms";
import { Router } from "@angular/router";
import { BaseForm } from "src/app/shared/components/base-form.component";
import { PASSWORD } from "src/app/shared/entity-url-mapping";
import { UserNameValidator } from "src/app/shared/validators/user-name.validator";
import { DataStoreFacade } from "src/app/store/data.facade";

@Component({
  selector: "app-user-name",
  templateUrl: "./user-name.component.html",
  styleUrls: ["./user-name.component.scss", "../user-info.component.scss"],
  providers: [UserNameValidator],
})
export class UserNameComponent extends BaseForm {
  constructor(
    private readonly _fb: FormBuilder,
    private readonly _router: Router,
    private readonly _userNameValidator: UserNameValidator,
    private readonly _dataStoreFacade: DataStoreFacade
  ) {
    super();
    this.form = this._fb.group({
      userName: [
        "",
        [
          Validators.required,
          Validators.minLength(3),
          Validators.maxLength(20),
        ],
        [this._userNameValidator.checkUserName.bind(this._userNameValidator)],
      ],
    });
  }

  public submitUserName() {
    this.form.markAllAsTouched();
    if (this.form?.valid) {
      this._dataStoreFacade.enterCorrectUserName();
      this._router.navigate([PASSWORD]);
    }
  }
}
