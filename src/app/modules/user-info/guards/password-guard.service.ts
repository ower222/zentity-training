import { Injectable } from "@angular/core";
import { CanActivate, Router } from "@angular/router";
import { firstValueFrom } from "rxjs";
import { USER_NAME } from "src/app/shared/entity-url-mapping";
import { DataStoreFacade } from "src/app/store/data.facade";

@Injectable()
export class PasswordGuardService implements CanActivate {
  constructor(
    private readonly _dataStoreFacade: DataStoreFacade,
    private readonly _router: Router
  ) {}
  async canActivate(): Promise<boolean> {
    await firstValueFrom(this._dataStoreFacade.hasFilledUserName$);
    if (await firstValueFrom(this._dataStoreFacade.hasFilledUserName$))
      return true;
    this._router.navigate([USER_NAME]);
    return false;
  }
}
