import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { ReactiveFormsModule } from "@angular/forms";
import { SharedComponentsModule } from "src/app/shared/components/shared-components.module";
import { PasswordGuardService } from "./guards/password-guard.service";
import { IntroductionComponent } from "./introduction/introduction.component";
import { PasswordComponent } from "./password/password.component";
import { UserInfoRoutingModule } from "./user-info-routing.module";
import { UserNameComponent } from "./user-name/user-name.component";

@NgModule({
  declarations: [IntroductionComponent, UserNameComponent, PasswordComponent],
  imports: [
    CommonModule,
    UserInfoRoutingModule,
    SharedComponentsModule,
    ReactiveFormsModule,
  ],
  providers: [PasswordGuardService],
})
export class UserInfoModule {}
