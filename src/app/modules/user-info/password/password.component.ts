import { Component } from "@angular/core";
import { FormBuilder } from "@angular/forms";
import { Router } from "@angular/router";
import { takeUntil, tap } from "rxjs";
import { BaseForm } from "src/app/shared/components/base-form.component";
import { PROFILE } from "src/app/shared/entity-url-mapping";
import { PasswordValidator } from "src/app/shared/validators/password.validator copy";
import { DataStoreFacade } from "src/app/store/data.facade";

@Component({
  selector: "app-password",
  templateUrl: "./password.component.html",
  styleUrls: ["./password.component.scss", "../user-info.component.scss"],
  providers: [PasswordValidator],
})
export class PasswordComponent extends BaseForm {
  public userName: string | undefined;
  constructor(
    private readonly _fb: FormBuilder,
    private readonly _router: Router,
    private readonly _passwordValidator: PasswordValidator,
    private readonly _dataStoreFacade: DataStoreFacade
  ) {
    super();
    this.form = this._fb.group(
      {
        password: [""],
        passwordVerify: [""],
      },
      {
        asyncValidator: [
          this._passwordValidator.checkPassword.bind(this._passwordValidator),
        ],
      }
    );

    this._dataStoreFacade.userName$
      .pipe(
        tap((userName) => (this.userName = userName)),
        takeUntil(this.ngUnsubscribe)
      )
      .subscribe();
  }

  public submitPassword() {
    this.form.markAllAsTouched();
    if (this.form.valid) {
      this._dataStoreFacade.enterCorrectPassword();
      this._router.navigate([PROFILE]);
    }
  }
}
