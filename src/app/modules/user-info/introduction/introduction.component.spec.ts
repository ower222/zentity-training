import { TestBed } from "@angular/core/testing";
import { RouterTestingModule } from "@angular/router/testing";
import { IntroductionComponent } from "./introduction.component";

describe("IntroductionComponent", () => {
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [RouterTestingModule],
      declarations: [IntroductionComponent],
    }).compileComponents();
  });

  it("should create the app", () => {
    const fixture = TestBed.createComponent(IntroductionComponent);
    const app = fixture.componentInstance;
    expect(app).toBeTruthy();
  });
});
