import { Component } from "@angular/core";
import { Router } from "@angular/router";
import { USER_NAME } from "src/app/shared/entity-url-mapping";

@Component({
  selector: "app-introduction",
  templateUrl: "./introduction.component.html",
  styleUrls: ["./introduction.component.scss"],
})
export class IntroductionComponent {
  constructor(private readonly _router: Router) {}

  public onStart() {
    this._router.navigate([USER_NAME]);
  }
}
