import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { PASSWORD, USER_NAME } from "src/app/shared/entity-url-mapping";
import { PasswordGuardService } from "./guards/password-guard.service";
import { IntroductionComponent } from "./introduction/introduction.component";
import { PasswordComponent } from "./password/password.component";
import { UserNameComponent } from "./user-name/user-name.component";

const routes: Routes = [
  {
    path: "",
    component: IntroductionComponent,
    data: { hideBackButton: false },
  },
  { path: USER_NAME, component: UserNameComponent },
  {
    path: PASSWORD,
    component: PasswordComponent,
    canActivate: [PasswordGuardService],
  },
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class UserInfoRoutingModule {}
