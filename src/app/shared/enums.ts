export enum SOCIAL_MEDIA {
  LINKED_IN = "linkedIn",
  TWITTER = "twitter",
  FACEBOOK = "facebook",
}

export enum SOCIAL_MEDIA_ICONS {
  LINKED_IN = "LinkedIn@3x.png",
  TWITTER = "Twitter@3x.png",
  FACEBOOK = "Facebook@3x.png",
}

export const SOCIAL_MEDIA_ICON_MAP: Map<SOCIAL_MEDIA, SOCIAL_MEDIA_ICONS> =
  new Map([
    [SOCIAL_MEDIA.LINKED_IN, SOCIAL_MEDIA_ICONS.LINKED_IN],
    [SOCIAL_MEDIA.FACEBOOK, SOCIAL_MEDIA_ICONS.FACEBOOK],
    [SOCIAL_MEDIA.TWITTER, SOCIAL_MEDIA_ICONS.TWITTER],
  ]);

export enum FORM_PROPERTIES {
  FIELD = "field",
  FORM = "form",
}

export enum FORM_INPUT_TYPE {
  PHONE = "phone",
  POSTAL_CODE = "postalCode",
}
