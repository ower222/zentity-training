import { Injectable } from "@angular/core";
import { FormControl } from "@angular/forms";
import { firstValueFrom } from "rxjs";
import { DataStoreFacade } from "src/app/store/data.facade";

@Injectable()
export class UserNameValidator {
  constructor(private readonly _dataStoreFacade: DataStoreFacade) {}

  async checkUserName(control: FormControl) {
    const userName = await firstValueFrom(this._dataStoreFacade.userName$);
    return userName === control.value ? null : { userNameMatch: false };
  }
}
