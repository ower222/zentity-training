import { Injectable } from "@angular/core";
import { AbstractControl } from "@angular/forms";
import { firstValueFrom } from "rxjs";
import { DataStoreFacade } from "src/app/store/data.facade";

@Injectable()
export class PasswordValidator {
  constructor(private readonly _dataStoreFacade: DataStoreFacade) {}

  async checkPassword(form: AbstractControl) {
    const password = form.get("password")?.value;
    const passwordVerify = form.get("passwordVerify")?.value;
    if (!password || !passwordVerify) return { emptyField: true };
    if (password !== passwordVerify) return { passwordMismatch: true };
    const userPassword = await firstValueFrom(
      this._dataStoreFacade.userPassword$
    );

    return userPassword === password ? null : { incorrectPassword: false };
  }
}
