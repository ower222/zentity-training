import { Directive, OnDestroy } from "@angular/core";
import { FormGroup } from "@angular/forms";
import { Subject } from "rxjs";

@Directive()
export class BaseForm implements OnDestroy {
  public form!: FormGroup;
  protected ngUnsubscribe = new Subject();

  ngOnDestroy(): void {
    this.ngUnsubscribe.next(undefined);
    this.ngUnsubscribe.complete();
  }
}
