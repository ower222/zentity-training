import {
  Component,
  EventEmitter,
  Input,
  Optional,
  Output,
  Self,
} from "@angular/core";
import { ControlValueAccessor, NgControl } from "@angular/forms";

@Component({
  selector: "app-input-form-control",
  templateUrl: "./input-form-control.component.html",
  styleUrls: ["./input-form-control.component.scss"],
})
export class InputFormControlComponent implements ControlValueAccessor {
  @Input() showDeleteLabel = true;
  @Input() label = "";
  @Input() editable = true;
  @Input() inputType = "";

  @Output() save = new EventEmitter();

  public editMode = false;
  public invalidState = false;

  onChange = (_value: string) => {};
  onTouched = () => {};
  touched = false;
  disabled = false;

  value = "";

  private _tempValue = "";

  constructor(@Self() @Optional() private control: NgControl) {
    this.control.valueAccessor = this;
  }

  public get invalid(): boolean {
    return this.control
      ? this.control.invalid
        ? this.control.invalid
        : false
      : false;
  }

  onValueChange(event: any) {
    this.markAsTouched();
    this.value = event?.target?.value;
  }

  writeValue(value: string): void {
    this.value = value;
  }

  registerOnChange(onChange: any): void {
    this.onChange = onChange;
  }

  registerOnTouched(onTouched: any) {
    this.onTouched = onTouched;
  }

  markAsTouched() {
    if (!this.touched) {
      this.onTouched();
      this.touched = true;
    }
  }

  setDisabledState(disabled: boolean) {
    this.disabled = disabled;
  }

  setEditMode(): void {
    this._tempValue = this.value;
    this.editMode = true;
  }

  cancelChanges(): void {
    this.value = this._tempValue;
    this.editMode = false;
    if (this.invalidState) {
      this.onChange(this.value);
      this.invalidState = this.invalid;
    }
  }

  saveChanges(): void {
    this.onChange(this.value);
    this.invalidState = this.invalid;
    if (this.invalid) return;
    this.editMode = false;
    this.save.emit();
  }

  deleteValue(): void {
    this.value = "";
    this.onChange(this.value);
    this.invalidState = this.invalid;
    this.save.emit();
  }
}
