import { Pipe, PipeTransform } from "@angular/core";
import { FORM_INPUT_TYPE } from "../../enums";

@Pipe({ name: "formatInputText" })
export class FormatInputTextPipe implements PipeTransform {
  transform(value: string | undefined, type: string): string | undefined {
    switch (type) {
      case FORM_INPUT_TYPE.PHONE: {
        if (value && value.charAt(0) !== "+") {
          const formattedValue = value.match(/.{1,3}/g);
          value = formattedValue ? formattedValue.join(" ") : "";
          return "+420 ".concat(value);
        }
        return value;
      }
      case FORM_INPUT_TYPE.POSTAL_CODE: {
        if (value) {
          return value.replace(/(?=.{3}$)/, " ");
        }
        return value;
      }
      default:
        return value;
    }
  }
}
