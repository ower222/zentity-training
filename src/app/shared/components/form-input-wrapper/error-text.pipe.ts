import { Pipe, PipeTransform } from "@angular/core";
import { FORM_PROPERTIES } from "../../enums";

@Pipe({ name: "errorText" })
export class ErrorTextPipe implements PipeTransform {
  transform(value: string, type: string): string {
    switch (type) {
      case FORM_PROPERTIES.FIELD: {
        switch (value) {
          case "required":
          case "maxlength":
          case "minlength":
            return "Ooops. Your username must be between 3 and 20 characters long.";
          case "userNameMatch":
            return "Ooops. Entered username does not exists.";
          default:
            return "Ooops. Entered value is not valid.";
        }
      }
      case FORM_PROPERTIES.FORM: {
        switch (value) {
          case "emptyField":
            return "Ooops. All fields are mandatory.";
          case "passwordMismatch":
            return "Ooops. Passwords does not match.";
          case "incorrectPassword":
            return "Ooops. Wrong password.";
          default:
            return "Ooops. Form is not valid.";
        }
      }
      default:
        console.error("Wrong form property type");
        return value;
    }
  }
}
