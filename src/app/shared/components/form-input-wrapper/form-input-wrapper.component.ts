import { Component, Input } from "@angular/core";
import { AbstractControl, FormControl } from "@angular/forms";
import { FORM_PROPERTIES } from "../../enums";

@Component({
  selector: "app-form-input-wrapper",
  templateUrl: "./form-input-wrapper.component.html",
  styleUrls: ["./form-input-wrapper.component.scss"],
})
export class FormInputWrapperComponent {
  @Input() formControlObj: AbstractControl = new FormControl();
  @Input() formControlType: string = FORM_PROPERTIES.FIELD;

  public get errors(): string[] {
    if (!this.formControlObj?.errors) return [];
    return Object.keys(this.formControlObj?.errors);
  }
}
