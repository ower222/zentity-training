import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { ErrorTextPipe } from "./form-input-wrapper/error-text.pipe";
import { FormInputWrapperComponent } from "./form-input-wrapper/form-input-wrapper.component";
import { IconWithTextComponent } from "./icon-with-text/icon-with-text.component";
import { InputFormControlComponent } from "./input-form-control/input-form-control.component";
import { FormatInputTextPipe } from "./input-form-control/input-text-format.pipe";

@NgModule({
  declarations: [
    ErrorTextPipe,
    IconWithTextComponent,
    FormInputWrapperComponent,
    InputFormControlComponent,
    FormatInputTextPipe,
  ],
  imports: [CommonModule],
  exports: [
    IconWithTextComponent,
    FormInputWrapperComponent,
    InputFormControlComponent,
  ],
  providers: [],
})
export class SharedComponentsModule {}
