import { Component } from "@angular/core";
import { Location } from "@angular/common";
import { IntroductionComponent } from "src/app/modules/user-info/introduction/introduction.component";

@Component({
  selector: "app-user-info-layout",
  templateUrl: "./user-info-layout.component.html",
  styleUrls: ["./user-info-layout.component.scss"],
})
export class UserInfoLayoutComponent {
  public showBackButton = true;

  constructor(private readonly _location: Location) {}

  public onRouterOutletActivate(event: any) {
    this.showBackButton = !(event instanceof IntroductionComponent);
  }

  back(): void {
    this._location.back();
  }
}
