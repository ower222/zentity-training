import { createReducer, on } from "@ngrx/store";
import {
  loadData,
  loadDataSuccess,
  logout,
  setCorrectPassword,
  setCorrectUserName,
  updateData,
} from "./data.actions";
import { UserProfile } from "./data.model";

export const initialState: UserProfile = new UserProfile();

export const dataReducer = createReducer(
  initialState,
  on(loadData, (state) => state),
  on(loadDataSuccess, (state, action) => {
    state = action.userProfile;
    return state;
  }),
  on(updateData, (state, data) => {
    const contact = { ...state.contact, ...data.contact };
    const address = { ...state.address, ...data.address };
    state = { ...state, ...data, contact, address };
    return state;
  }),
  on(setCorrectUserName, (state) => {
    return { ...state, correctUserName: true };
  }),
  on(setCorrectPassword, (state) => {
    return { ...state, correctPassword: true };
  }),
  on(logout, (state) => {
    return { ...state, correctPassword: false, correctUserName: false };
  })
);
