import { createSelector } from "@ngrx/store";
import { UserProfile } from "./data.model";

const userData = (state: { data: UserProfile }): UserProfile => {
  return state.data;
};
const password = (state: { data: UserProfile }): string | undefined =>
  state.data.password;
const userName = (state: { data: UserProfile }): string | undefined =>
  state.data.userName;
const isLoggedIn = (state: { data: UserProfile }): boolean =>
  !!state.data.correctUserName && !!state.data.correctPassword;
const filledUserName = (state: { data: UserProfile }): boolean =>
  !!state.data.correctUserName;

export const selectUserData = createSelector(
  userData,
  (state: UserProfile) => state
);

export const selectPassword = createSelector(
  password,
  (state: string | undefined) => state
);

export const selectUserName = createSelector(
  userName,
  (state: string | undefined) => state
);

export const selectIsLoggedIn = createSelector(
  isLoggedIn,
  (state: boolean) => state
);

export const selectFilledUserName = createSelector(
  filledUserName,
  (state: boolean) => state
);
