import { createAction, props } from '@ngrx/store';
import { UserProfile } from './data.model';

export const loadData = createAction('[Data Store] Load data');
export const loadDataSuccess = createAction('[Data Store] Load data successed', props<{userProfile: UserProfile}>());
export const setCorrectUserName = createAction('[User Info] Set correct username');
export const setCorrectPassword = createAction('[User Info] Set correct password');
export const updateData = createAction('[Profile] Update profile data', props<UserProfile>());
export const logout = createAction('[Profile] Logout');