import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { catchError, EMPTY, map, mergeMap } from "rxjs";
import { loadData, loadDataSuccess } from "./data.actions";
import { UserDataModel, UserProfile } from "./data.model";

@Injectable()
export class DataEffects {
  loadData$ = createEffect(() => this.actions$.pipe(
    ofType(loadData),
    mergeMap(() => this.http.get('../assets/data.json')
      .pipe(
        map(response => new UserProfile(response as UserDataModel)),
        map(resp => ({ type: loadDataSuccess.type , userProfile: resp })),
        catchError(() => EMPTY)
      ))
    )
  );
 
  constructor(
    private actions$: Actions,
    private http: HttpClient
  ) {
  }
}