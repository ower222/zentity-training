import { SOCIAL_MEDIA, SOCIAL_MEDIA_ICON_MAP } from "../shared/enums";

export interface UserDataModel {
    onboarded: boolean;
    user: User;
}

export interface SocialNetworks {
    id: string;
    name: string;
    url: string; 
    icon?: string;
}

export interface Address {
    streetName: string;
    streetNumber: string;
    suburb: string;
    postalCode: string;
    addressString: string;
}

export interface Location {
    name: string;
    address: Address;
}

export interface Contact {
    email: string;
    phoneNumber: string;
    locations: Location[];
    socialNetworks: SocialNetworks[];
}

export interface User {
    name: string;
    surname: string;
    userName: string;
    password: string;
    contact: Contact;
}

export class UserProfile {
    name: string = '';
    userName: string = '';
    password: string = '';
    address: ProfileAddress = new ProfileAddress();
    contact: ProfileContact = new ProfileContact();
    correctUserName: boolean = false;
    correctPassword: boolean = false;

    constructor(data: UserDataModel | undefined = undefined) {
        if (!data) return;
        const userData = data?.user;
        const userAddress = userData?.contact.locations.find(loc => loc.name === 'address')?.address;
        const userContact = userData?.contact;
        this.name = userData?.name + ' ' + userData?.surname;
        this.userName = userData?.userName;
        this.password = userData?.password;
        this.address = new ProfileAddress(userAddress);
        this.contact = new ProfileContact(userContact);
    }
}

export class ProfileAddress {
    address: string = '';
    city: string = '';
    postalCode: string = '';

    constructor(data: Address | undefined = undefined) {
        if (!data) return;
        this.address = data?.streetName + ' ' + data?.streetNumber;
        this.city = data?.suburb;
        this.postalCode = data?.postalCode;
    }
}

export class ProfileContact {
    email: string = '';
    phone: string = '';
    socialNetworksString: string = '';
    socialNetworks: SocialNetworks[] = [];

    constructor(data: Contact | undefined = undefined) {
        if (!data) return;
        this.email = data?.email
        this.phone = data?.phoneNumber;
        this.socialNetworksString = data.socialNetworks.map(network => network.name).join(', ');
        this.socialNetworks = data.socialNetworks;
        this.socialNetworks.forEach(sc => sc.icon = SOCIAL_MEDIA_ICON_MAP.get(sc.id as SOCIAL_MEDIA));
    }
}
