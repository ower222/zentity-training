import { Injectable } from "@angular/core";
import { Store } from "@ngrx/store";
import {
  loadData,
  logout,
  setCorrectPassword,
  setCorrectUserName,
  updateData,
} from "./data.actions";
import { UserProfile } from "./data.model";
import {
  selectFilledUserName,
  selectIsLoggedIn,
  selectPassword,
  selectUserData,
  selectUserName,
} from "./data.selector";

@Injectable()
export class DataStoreFacade {
  constructor(public readonly store: Store<{data: UserProfile}>) {}

  public userDetail$ = this.store.select(selectUserData);
  public userPassword$ = this.store.select(selectPassword);
  public userName$ = this.store.select(selectUserName);
  public isLoggedIn$ = this.store.select(selectIsLoggedIn);
  public hasFilledUserName$ = this.store.select(selectFilledUserName);

  public retrieveUserData(): void {
    this.store.dispatch(loadData());
  }

  public updateProfileData(data: UserProfile): void {
    this.store.dispatch(updateData(data));
  }

  public enterCorrectUserName(): void {
    this.store.dispatch(setCorrectUserName());
  }

  public enterCorrectPassword(): void {
    this.store.dispatch(setCorrectPassword());
  }

  public logout(): void {
    this.store.dispatch(logout());
  }
}
